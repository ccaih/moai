from rdflib import Namespace

MARCREL = Namespace(u'http://www.loc.gov/loc.terms/relators/')
VIVO = Namespace(u'http://vivoweb.org/ontology/core#')
CLD = Namespace(u'http://purl.org/cld/terms/')
ANDS = Namespace(u'http://purl.org/ands/ontologies/vivo/')
FOR08 = Namespace(u'http://purl.org/asc/1297.0/2008/for/')
DCMI = Namespace(u'http://purl.org/dc/dcmitype/')
XSD = Namespace(u'http://www.w3.org/2001/XMLSchema#')
