#from zope.interface import implements
#from moai.interfaces import IContentProvider
from org.terranova.moai.content import RDFContentObject

from ordf.handler import init_handler
from ordf.graph import Graph
from rdflib import URIRef, Literal, Namespace
from pkg_resources import resource_string

from ordf.namespace import DCES, DC, RDF, BIBO, FOAF, XSD, SKOS
from org.terranova.moai.namespace import MARCREL, CLD, ANDS, FOR08, DCMI
CVOCAB = Namespace(u"http://namespaces.griffith.edu.au/collection_vocab#")

class ORDFContentProvider(object):

    #implements(IContentProvider)

    def __init__(self, config):
        self.config = config
        self.handler = init_handler(config)
        self.harvester = URIRef(config['harvesteruri'])
        data = self.handler.query("DESCRIBE %s" % self.harvester.n3())
        self.originatingSource = data.value(self.harvester, ANDS.originatingSource) or config['originatingsource']
        self.groupDescription = data.value(self.harvester, ANDS.groupDescription) or config['groupdescription']

        self.allowableTypes = ((FOAF.Person), (FOAF.Organisation), (FOAF.Group), (DCMI.Collection), (CVOCAB.Collection) )

       
    def set_logger(self, log):
        """Set the logger instance for this class
        """
        self.log = log

    def update(self, from_date=None):
        """Harvests new content added since from_date
        returns a list of content_ids that were changed/added,
        this should be called before get_contents is called
        """
        #original select query
        #selectquery = ("SELECT DISTINCT ?item WHERE  {  ?item %s %s . }" % (ANDS.harvestedBy.n3(), self.harvester.n3()))

        #set up a filter to only retrieve valid types for feed
        filterquery = ("FILTER ( ?type = <%s> || ?type = <%s> || ?type = <%s> || ?type = <%s> || ?type = <%s> )" % ( CVOCAB.Collection, DCMI.Collection, FOAF.Person, FOAF.Organisation, FOAF.Group ))
        selectquery = ("SELECT DISTINCT ?item WHERE  {  ?item %s %s . ?item <%s> ?type .  %s }" % (ANDS.harvestedBy.n3(), self.harvester.n3(), RDF.type, filterquery ))

        result = self.handler.query(selectquery)
        #self._content = [r['item'] for r in result]
        #print selectquery
        self._content = [r[0] for r in result]

        return self._content

    def count(self):
        """Returns number of content objects in the repository
        returns None if number is unknown, this should not be
        called before update is called
        """
        return len(self._content)

    def get_content_ids(self):
        """returns a list/generator of content_ids
        """
        return self._content

    def get_content_by_id(self, id):
        """Return content of a specific id
        """
        # assume id is URIRef instance
        data = self.handler.get(id)
        if not len(data):
            # it's not a single graph... may be we should run a describe query
            data = Graph(identifier=id)
            data += self.handler.query("DESCRIBE %s" % id.n3())

        #get additional relations with reverse queries
        managesQuery = ("CONSTRUCT { %s %s ?x . } WHERE { ?x %s %s  }" % ( id.n3(), ANDS['isPrincipalInvestigatorOf'].n3(), DC['creator'].n3(), id.n3() ))
        data += self.handler.query(managesQuery)
        ownerQuery = ("CONSTRUCT { %s %s ?x . } WHERE { ?x %s %s  }" % ( id.n3(), ANDS['isOwnerOf'].n3(), DC['owner'].n3(), id.n3() ))                        
        data += self.handler.query(ownerQuery)
        contributorQuery = ("CONSTRUCT { %s %s ?x . } WHERE { ?x %s %s  }" % ( id.n3(), ANDS['isCollectorOf'].n3(), DC['contributor'].n3(), id.n3() ))                        
        data += self.handler.query(contributorQuery)    
        custodianQuery = ("CONSTRUCT { %s %s ?x . } WHERE { ?x %s %s  }" % ( id.n3(), ANDS['isManagerOf'].n3(), DC['custodian'].n3(), id.n3() ))                        
        data += self.handler.query(custodianQuery)    

        #print(data.serialize(format="n3"))
       
        return data
