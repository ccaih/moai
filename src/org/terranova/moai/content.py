from datetime import datetime
from urlparse import urlsplit
#from zope.interface import implements
#from moai.interfaces import IContentObject
from ordf.namespace import DCES, DC, RDF, BIBO, FOAF, XSD, SKOS
from rdflib import URIRef, Literal, Namespace
from org.terranova.moai.namespace import MARCREL, CLD, ANDS, FOR08, DCMI, VIVO
from org.terranova.moai.utils import Period

# FIXME: get this from central place?
CVOCAB = Namespace(u"http://namespaces.griffith.edu.au/collection_vocab#")


def _uri_to_oai(uri):
    parts = urlsplit(uri)
    return '%s%s' % (parts.netloc, parts.path.replace('/', ':'))


def _uri_to_key(uri):
    parts = urlsplit(uri)
    return 'terranova.org.au/%s' % (parts.path[1:].replace('/', ':'))


def _get_identifier_list(data, subject, local_fallback=True):
    idinfo = []
    for idprop, idtype, asUri, uriprefix in ((BIBO.handle, 'handle', False, u'http://hdl.handle.net/%s'),
                                             (BIBO.doi, 'doi', False, u'http://dx.doi.org/%s'),
                                             (BIBO.uri, 'uri', False, None),
                                             (BIBO.isbn, 'isbn', False, None),
                                             (DCES.identifier, 'uri', True, None),
                                             (ANDS.nla, 'AU-ANL:PEAU', False, None),
                                             (BIBO.isbn, 'isbn', False, None)):
        for obj in data.objects(subject, idprop):
            val = unicode(obj)
            if asUri:
                idtype = 'uri'
                if uriprefix:
                    val = uriprefix % val
            idinfo.append({'type': idtype,
                           'value': val})
    if not idinfo and isinstance(subject, URIRef) and local_fallback:
        # if we don't know anything and the subject is a URI ... treat it as uri
        idinfo.append({'type': 'uri',
                       'value': unicode(subject)})
    return idinfo


def _get_itemtype(data, subject):
    if (subject, RDF.type, FOAF.Person) in data:
        return ('party', 'person')
    if (subject, RDF.type, FOAF.Organisation) in data:
        return ('party', 'group')
    if (subject, RDF.type, FOAF.Group) in data:
        return ('party', 'group')
    if (subject, RDF.type, DCMI.Collection) in data:
        return ('collection', 'collection')
    if (subject, RDF.type, CVOCAB.Collection) in data:
        return ('collection', 'collection')
    #if no type matched - return undefined
    return ('undefined', 'undefined')
        


def _extract_identifier(provider, metadata, data, subject):
    '''
    aadd list of
    {'type': '', 'value': '' }
    dictionaries with key 'identifier' into metadat
    '''

    idinfo = _get_identifier_list(data, subject)
    if idinfo:
        metadata['rif_identifier'] = idinfo


def _extract_name(provider, metadata, data, subject):
    '''
    insert list of
    {'type': '',
     'lang': '',
     'dateFrom':'',
     'dateTo':'',
     'namePart': [{'type':'',
                   'value':''}]
    }
    with key 'name' into metadata
    '''
    nameinfo = []
    for nameprop, nametype in ((DC.title, 'primary'),
                               (FOAF.name, 'primary'),
                               (DCES.alternative, 'alternative'),
                               (DC.alternative, 'alternative')):
        for obj in data.objects(subject, nameprop):
            nameinfo.append({'type': nametype,
                             'namePart': [{'value': unicode(obj)}]})
    if nameinfo:
        metadata['rif_name'] = nameinfo


def _extract_location(provider, metadata, data, subject):
    '''
    self.metadata['location'] = {'electronic' :[{'type': '',
                                                 'value': ''}],
                                 'physical': [{'type': '', # addressParts
                                               'value': ''}],
                                }
    '''
    locationinfo = {}
    # the subject uri is one electronic address
    #locationinfo['electronic'] = [{'type': 'url', 'value': unicode(subject)}]
    for locprop, loctype in ((DC.website, 'url'),):
        for obj in data.objects(subject, locprop):
            if 'electronic' not in locationinfo:
                locationinfo['electronic'] = []
            locationinfo['electronic'].append({'type': loctype,
                                             'value': unicode(obj)
                                            })


    fullAddress = ""
    for locprop, loctype in ((VIVO.address1, 'streetAddress'),
                             (VIVO.address2, 'streetAddress'),
                             (VIVO.address3, 'streetAddress'),
                             (VIVO.addressCity, 'streetAddress'),
                             (VIVO.addressState, 'streetAddress'),
                             (VIVO.addressPostalCode, 'streetAddress'),
                             (VIVO.addressCountry, 'streetAddress')):
        
        for obj in data.objects(subject, locprop):
            fullAddress += unicode(obj) + " "
        
    if 'physical' not in locationinfo:
        locationinfo['physical'] = []
    if fullAddress:
        locationinfo['physical'].append({
            'type': loctype,
            'addressPart': [{
                'type': 'text',
                'value': fullAddress.strip()
            }]
        })
    

    if locationinfo:
        metadata['rif_location'] = locationinfo
        


def _extract_coverage(provider, metadata, data, subject):
    # {'covtype': 'temporal|spatial',
    #  'type': spatial_type,
    #  'value': spatial_value
    #  'date': {'type': '',
    #           'dateFormat': '',
    #           'value': ''},
    # 'text': 'value'}
    covinfo = []
    for covprop, covtype in ((DC.temporal, 'temporal'),):
        for obj in data.objects(subject, covprop):
            if obj.datatype == DC.Period:
                dateinfo = {'covtype': 'temporal',
                            'type': 'W3CDTF'}
                period = Period(unicode(obj))
                if period.start:
                    dateinfo['start'] = period.start
                if period.end:
                    dateinfo['end'] = period.end
                covinfo.append(dateinfo)
            elif obj.datatype == XSD.gYear:
                covinfo.append({'covtype': 'temporal',
                                'type': 'W3CDTF',
                                'start': unicode(obj)})
            else:
                covinfo.append({'covtype': 'temporal',
                                'type': 'text',
                                'value': unicode(obj)})
    for covprop, covtype in ((DC.spatial, 'spatial'),):
        for obj in data.objects(subject, covprop):
            # TODO: support additional formats and props form oupdated ands ontology
            if "POLYGON" in obj:
                #Converting WKT string to RDA kmlPolyCoords Value
                wktstring = obj.replace("POLYGON","").replace("(","").replace(")","").replace(",","|").replace(" ",",").replace("|"," ")
                covinfo.append({'covtype': 'spatial',
                            'value': unicode(wktstring), 'type': 'kmlPolyCoords'})
            elif obj.datatype == DC.Box:
                covinfo.append({'covtype': 'spatial',
                                'value': unicode(obj), 'type': 'iso19139dcmiBox'})
            else:
                covinfo.append({'covtype': 'spatial',
                                'value': unicode(obj), 'type': 'text'})
    if covinfo:
        metadata['rif_coverage'] = covinfo


def _extract_relatedobject(provider, metadata, data, subject):
    # 'key': '',
    # 'relation': {'description': {'lang':'',
    #                              'value': ''},
    #             'url': '',
    #             'type': '' }
    #  }
    relinfo = []
    for relprop, reltype in ((ANDS.isManagedBy, 'isManagedBy'),
                             (DC.owner, 'isOwnedBy'),
                             (DC.creator, 'hasPrincipalInvestigator'),
                             (DC.custodian, 'isManagedBy'),
                             (DC.contributor, 'hasCollector'),
                             (ANDS.isPrincipalInvestigatorOf, 'isPrincipalInvestigatorOf'),
                             (ANDS.isManagerOf, 'isManagerOf'),
                             (ANDS.isOwnedBy, 'isOwnedBy'),
                             (ANDS.isOwnerOf, 'isOwnerOf'),
                             (ANDS.isOutputOf, 'isOutputOf'),
                             (ANDS.hasOutput, 'hasOutput'),
                             (ANDS.isParticipantIn, 'isParticipantIn'),
                             (ANDS.hasParticipant, 'hasParticipant'),
                             (ANDS.isPointOfContactFor, 'isPointOfContact'),
                             (ANDS.hasPointOfContact, 'pointOfContact'),
                             (ANDS.isCollectorOf, 'isCollectorOf'),
                             (ANDS.hasCollector, 'hasCollector'),
                             (ANDS.hasAssociationWith, 'hasAssociationWith'),
                             (ANDS.supports, 'supports'),
                             (ANDS.makesAvailable, "makesAvailable")):
        for obj in data.objects(subject, relprop):
            
            
            if isinstance(obj, URIRef):
                relinfo.append({'key': _uri_to_key(obj),
                                'relation' : {'type': reltype}})
            elif isinstance(obj, Literal):
                if reltype == "hasAssociationWith" or reltype == "makesAvailable":
                    #print unicode(obj)
                    relinfo.append({'key': unicode(obj),
                                    'relation' : {'type': reltype}})
                    
  
    if relinfo:
        metadata['rif_relatedobject'] = relinfo


def _extract_subject(provider, metadata, data, subject):
    # 'type': '',
    # 'termIdentifier': '',
    # 'lang': '',
    # 'value': ''}
    subjectinfo = []
    

    for prop in (DCES.subject, DC.subject):
        for obj in data.objects(subject, prop):
            # FIXME: extend this here so support all sorts of subject identifiers.
            #        and enter the else part only if obj is a Literal.
            if obj.startswith(FOR08):
                subjectinfo.append({'type': 'anzsrc-for',
                                    'value': obj.rsplit('/', 1)[-1]})
            else:
                subjectinfo.append({'type': 'local',
                                    'value': unicode(obj)})
    if subjectinfo:
        metadata['rif_subject'] = subjectinfo
    


def _extract_description(provider, metadata, data, subject):
    # {'type': '',
    #  'lang': '',
    #  'value': ''}
    descrinfo = []
    for descprop, desctype in ((DC['description'], 'full'),
                               (BIBO['shortDescription'], 'brief'),
                               (SKOS['note'], 'note'),
                               (DC.sensitivitystatement, 'sensitivityStatement')):
        for obj in data.objects(subject, descprop):
            descrinfo.append({'type': desctype,
                              'value': unicode(obj)}),
    if descrinfo:
        metadata['rif_description'] = descrinfo


def _extract_rights(provider, metadata, data, subject):
    # {'rightsStatemnt': {'rightsUri': '',
    #                    'value': ''},
    # 'licence': {'type': '',
    #             'rightsUri': '',
    #             'value': ''},
    # 'accessRights', {'type': '',
    #                  'rightsUri': '',
    #                  'value': ''}
    # }
    rightinfo = {}
    for rightprop, righttype in (
            (DC.accessRights, 'accessRights'),
            (DC.rights, 'rightsStatement')):
        for obj in data.objects(subject, rightprop):
            if isinstance(obj, Literal):
                rightinfo[righttype] = {'value': unicode(obj)}
            else:
                rd = {}
                value = provider.parties.value(obj, DC.description)
                if value:
                    rd['value'] = value
                uri = provider.parties.value(obj, BIBO.uri)
                if uri:
                    rd['rightsUri'] = uri
                rightinfo[righttype] = rd
    if rightinfo:
        metadata['rif_rights'] = rightinfo


def _extract_existencedates(provider, metadata, data, subject):
    # only for non collection types
    # {'start' startDate, endDate
    #  'end'
    #  'format' }
    existenceinfo = []
    obj = data.value(subject, DC.created)
    if obj:
        if obj.datatype == XSD.gYear:
            existenceinfo.append({'start': unicode(obj),
                                  'format': 'W3CDTF'})
        elif obj.datatype == DC.Period:
            period = Period(unicode(obj))
            dates = {'format': 'W3CDTF'}
            if period.start:
                dates['start'] = period.start
            if period.end:
                dates['end'] = period.end
            existenceinfo.append(dates)
    if existenceinfo:
        metadata['rif_existence'] = existenceinfo


def _extract_relatedinfo(provider, metadata, data, subject):
    # {'identifier': {'type': '',
    #                 'value': ''},
    #  'title': '',
    #  'notes': '',
    #  'type': ''}
    relinfo = []
    for relprop, reltype in ((DC.isReferencedBy, 'publication'),
                             (FOAF.webpage, 'website')):
        for obj in data.objects(subject, relprop):
            idinfo = _get_identifier_list(data, obj, False)
            title = data.label(obj)
            # TODO: check in more detail for complete data
            info = {'type': reltype}
            if title:
                info['title'] = title
            if idinfo:
                info['identifier'] = idinfo
                relinfo.append(info)
    if relinfo:
        metadata['rif_relatedInfo'] = relinfo


def _extract_citationinfo(provider, metadata, data, subject):
    # {'fullCitation': {'style': '',
    #                   'value': ''},
    #  # 'citationMetadata': {'identifier': ..]
    #  }
    pass


class RDFContentObject(object):

    #implements(IContentObject)

    #dc-fields:
    # 'title', 'creator', 'subject', 'description',
    # 'publisher', 'contributor', 'type', 'format',
    # 'identifier', 'source', 'language', 'date',
    # 'relation', 'coverage', 'rights']:

    #rif-fields:
    # rif_type .... RIF-CS Object type
    # rif_subtype ... Sub type within RIF-CS object

    def __init__(self, provider):
        self.provider = provider
        self.id = None
        self.modified = None
        self.deleted = None
        self.data = None
        self.sets = None

    def update(self, data):
        """Called by IContentProvider, to fill the object with data
        """
        self.data = data
        # extract data
        subject = data.identifier
        self.id = _uri_to_oai(subject)
        self.modified = datetime.utcnow()
        self.deleted = False

        itemtype, subtype = _get_itemtype(data, subject)
        self.metadata = {}
      
        


            

        

        # fixed fields:
        self.metadata['rif_key'] = _uri_to_key(subject)
        self.metadata['rif_group'] = self.provider.groupDescription
        self.metadata['rif_originatingSource'] = self.provider.originatingSource
        self.metadata['rif_object'] = {'value': itemtype,
                                       'type': subtype,
                                       #'dateModified': '',
                                       }

        if itemtype == 'collection':
            self.updateCollection(data, subject)
        elif itemtype == 'party':
            self.updateParty(data, subject)

    def updateCollection(self, data, subject):
        self.sets = {u'Collections': {'name': u'Collections',
                                      'description': u'All Collections'}}

        # collection specific part
        _extract_identifier(self.provider, self.metadata, data, subject)
        _extract_name(self.provider, self.metadata, data, subject)
        # this should become location/address
        _extract_location(self.provider, self.metadata, data, subject)
        _extract_coverage(self.provider, self.metadata, data, subject)
        _extract_relatedobject(self.provider, self.metadata, data, subject)
        _extract_subject(self.provider, self.metadata, data, subject)
        _extract_description(self.provider, self.metadata, data, subject)
        _extract_rights(self.provider, self.metadata, data, subject)
        _extract_relatedinfo(self.provider, self.metadata, data, subject)
        _extract_citationinfo(self.provider, self.metadata, data, subject)

    def updateParty(self, data, subject):
        self.sets = {
            u'Party': {
                'name': u'Party',
                'description': u'All Parties'
            }
        }

        _extract_identifier(self.provider, self.metadata, data, subject)
        _extract_name(self.provider, self.metadata, data, subject)

        # this should become location/address
        _extract_location(self.provider, self.metadata, data, subject)
        _extract_coverage(self.provider, self.metadata, data, subject)
        _extract_relatedobject(self.provider, self.metadata, data, subject)
        _extract_subject(self.provider, self.metadata, data, subject)
        _extract_description(self.provider, self.metadata, data, subject)
        _extract_rights(self.provider, self.metadata, data, subject)
        _extract_relatedinfo(self.provider, self.metadata, data, subject)
        _extract_citationinfo(self.provider, self.metadata, data, subject)
