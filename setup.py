#!/usr/bin/env python


from setuptools import setup, find_packages


version = '1.1dev'


setup(
    name='org.terranova.moai',
    version=version,
    description="MOAI configuration and plugins for Terranova",
    #long_description=open("README.txt").read() + "\n" +
    #                 open(os.path.join("docs", "HISTORY.txt")).read(),
    # Get more strings from
    # http://pypi.python.org/pypi?:action=list_classifiers
    #classifiers=[
    #  "Framework :: Plone",
    #  "Programming Language :: Python",
    #  ],
    keywords='',
    author='',
    author_email='',
    #url='http://svn.plone.org/svn/collective/',
    #license='GPL',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['org', 'org.terranova'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'setuptools',
        'ordf',
        'MOAI',
        'gu.moai.rifcs',
    ],
    entry_points="""
    [moai.content]
    moai_terranova = org.terranova.moai.content:RDFContentObject

    [moai.provider]
    moai_terranova = org.terranova.moai.provider:ORDFContentProvider
    """,
)
